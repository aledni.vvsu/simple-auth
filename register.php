<?php

include __DIR__ . '/functions.php';

function register (): ?string {
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        return null;
    }

    $username = $_POST['username'] ?? null;
    $password = $_POST['password'] ?? null;
    $repeatPassword = $_POST['repeat_password'] ?? null;

    if ( ! $username) {
        return 'Логин обязателен для заполнения';
    }

    if ( ! $password) {
        return 'Пароль обязателен для заполнения';
    }

    if ( ! $repeatPassword) {
        return 'Повторите пароль';
    }

    if ($password !== $repeatPassword) {
        return 'Пароли не совпадают';
    }

    $pdo = getConn();
    $user = getUserByUsername($pdo, $username);

    if ($user) {
        return 'Выберите другой логин';
    }

    createUser($pdo, $username, $password);
    redirect('/login.php');

    return null;
}

$message = register();

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Регистрация пользователя</title>

    <link rel="stylesheet" href="styles.css" />
</head>
<body>
    <div class="simple-auth">
        <form action="register.php" method="post">
            <h1>Регистрация</h1>
            <div class="simple-auth__message"><?php echo $message ?></div>
            <input type="text" name="username" placeholder="Логин" />
            <input type="password" name="password" placeholder="Пароль" />
            <input type="password" name="repeat_password" placeholder="Повторите пароль" />
            <div class="simple-auth__controls">
                <a href="login.php">Войти</a>
                <button>Зарегистрироваться</button>
            </div>
        </form>
    </div>
</body>
</html>