<?php

// Функция создаем соединение с базой данных
function getConn(): PDO {
    return new PDO(
        'mysql:hostname=locahost;dbname=simple_auth',
        'root',
        ''
    ); // Передаем в объект реквизиты подключения к базе данных
}

// Функция создает нового пользователя
function createUser(PDO $pdo, $login, $password): bool {
    $stmt = $pdo->prepare('
        INSERT INTO users (username, password) 
        VALUE (:login, :password)
    '); // Подготавливаем sql запрос на добавление пользователя, указываем параменты :login и :password

    return $stmt->execute([
        'login' => $login,
        'password' => password_hash($password, PASSWORD_BCRYPT), // хэшируем пароль
    ]); // Выполняем запрос и связываем параметры login с переменной $login, и password с переменной $password
}

// Функция получает пользователя по логину
function getUserByUsername(PDO $pdo, string $username): ?array {
    $stmt = $pdo->prepare('SELECT * FROM users WHERE username = :username'); // Подготавливаем sql запрос
    $stmt->execute(['username' => $username]); // Связываем параметр id с переменной $id
    return $stmt->fetch() ?: null; // Извлекаем данные пользователя в массив, либо вовращаем null
}

// Функция получает пользователя по идентификатору
function getUserById(PDO $pdo, int $id): ?array {
    $stmt = $pdo->prepare('SELECT * FROM users WHERE id = :id'); // Подготавливаем sql запрос
    $stmt->execute(['id' => $id]); // Связываем параметр id с переменной $id
    return $stmt->fetch() ?: null; // Извлекаем данные пользователя в массив, либо вовращаем null
}

// Функция перенаправляет пользователя на другой ресурс (страницу)
function redirect(string $resource): void {
    header('Location: ' . $resource); // Отправляем заголовок location
    die();
}

// Функция запускает сессию
function startSession() {
    if (session_status() === PHP_SESSION_DISABLED) { // Проверяем включены ли сессии на сервере
        echo 'Сессии выключены'; // Если отключены, выводим сообщение
        die(); // И завершаем выполнение скрипта
    }

    if (session_status() === PHP_SESSION_NONE) { // Если сессия отключена
        session_start(); // Запускаем её
    }
}